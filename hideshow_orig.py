#!/usr/bin/env python
'''
hideshow_orig.py - toggle visibility of linked objects (move to/from defs)

Copyright (C) 2015, ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

# system modules
# internal modules
import inkex


def is_use(node):
    '''
    parameters: node
    checks whether node is a <use> element
    returns: true|false
    '''
    return node.tag == inkex.addNS('use', 'svg')


def is_symbol(node):
    '''
    parameters: node
    checks whether node is a <symbol> definition
    returns: true|false
    '''
    return node.tag == inkex.addNS('symbol', 'svg')


def is_path(node):
    '''
    parameters: node
    checks whether node is a <path> element
    returns: true|false
    '''
    return node.tag == inkex.addNS('path', 'svg')


def is_offset(node):
    '''
    parameters: node
    checks whether node is a offset path
    returns: true|false
    '''
    if is_path(node):
        return node.get(inkex.addNS('type', 'sodipodi')) == 'inkscape:offset'


def is_text(node):
    '''
    parameters: node
    checks whether node is a <text> element
    returns: true|false
    '''
    return node.tag == inkex.addNS('text', 'svg')


def is_flowRoot(node):
    '''
    parameters: node
    checks whether node is flowed text
    returns: true|false
    '''
    return node.tag == inkex.addNS('flowRoot', 'svg')


def is_textPath(node):
    '''
    parameters: node
    checks whether node is a text put on path (/text/textPath)
    returns: true|false
    '''
    if is_text(node):
        return true if node.find(inkex.addNS('textPath', 'svg')) is not None else False


def get_orig_from_href(node, document):
    '''
    parameters: node, document
    searches for href attribute
    returns: node of original object or None
    '''
    if inkex.addNS('href', 'xlink') in node.attrib:
        href_id = node.get(inkex.addNS('href', 'xlink'))[1:]
        path = './/svg:*[@id="{0}"]'.format(href_id)
        try:
            return document.getroot().xpath(path, namespaces=inkex.NSS)[0]
        except IndexError:
            return None


def get_orig_from_use(node, document):
    '''
    parameters: node, document
    checks whether node is a <use> element referencing an object in drawing
    returns: node of original object or None
    '''
    return get_orig_from_href(node, document)


def get_orig_from_offset(node, document):
    '''
    parameters: node, document
    checks whether node is a offset path referencing an object in drawing
    returns: node of original object or None
    '''
    return get_orig_from_href(node, document)


def get_orig_from_textPath(node, document):
    '''
    parameters: node, document
    checks whether node is text on path
    returns: node of referenced path element or None
    '''
    text_path = node.find(inkex.addNS('textPath', 'svg'))
    if text_path is not None:
        return get_orig_from_href(text_path, document)


def get_orig_from_flowRegion(node, document):
    '''
    parameters: node, document
    checks whether flowRegion contains <use> element
    returns: node of referenced frame or None
    '''
    flow_region = node.find(inkex.addNS('flowRegion', 'svg'))
    if flow_region is not None:
        frame = flow_region.find(inkex.addNS('use', 'svg'))
        if frame is not None:
            return get_orig_from_href(frame, document)


def get_node_from_defs(node, document):
    '''
    parameters: node, document
    checks whether node is in defs
    returns: node or None
    '''
    path = '/svg:svg//svg:defs/*[@id="{0}"]'.format(node.get('id'))
    try:
        return document.xpath(path, namespaces=inkex.NSS)[0]
    except IndexError:
        return None


def is_hidden(node, document):
    '''
    parameters: node, document
    checks whether node is in defs
    returns: true|false
    '''
    return True if get_node_from_defs(node, document) is not None else False


def is_visible(node, document):
    '''
    parameters: node, document
    checks whether node is not in defs
    returns: true|false
    '''
    return True if get_node_from_defs(node, document) is None else False


class HideShowOriginal(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("--show_mode",
                                     action="store", type="string",
                                     dest="show_mode", default="before",
                                     help="Specify index of orig restored from defs")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def hide_orig(self, node, orig):
        # TODO: check for transforms to be compensated
        self.document.getroot().find(inkex.addNS('defs', 'svg')).append(orig)

    def show_orig(self, node, orig, show_mode="before"):
        # TODO: check for transforms to be compensated
        parent = node.getparent()
        index = parent.index(node)
        if show_mode == "before":
            parent.insert(index, orig)
        elif show_mode == "after":
            parent.insert(index + 1, orig)
        elif show_mode == "first":
            parent.insert(0, orig)
        else:  # show_mode == "last"
            parent.append(orig)

    def toggle_visibility(self, node, orig):
        if is_visible(orig, self.document):
            self.hide_orig(node, orig)
        else:
            self.show_orig(node, orig, self.options.show_mode)

    def effect(self):
        for id_, node in self.selected.iteritems():
            orig = None
            if is_use(node):
                orig = get_orig_from_use(node, self.document)
            elif is_text(node):
                orig = get_orig_from_textPath(node, self.document)
            elif is_flowRoot(node):
                orig = get_orig_from_flowRegion(node, self.document)
            elif is_offset(node):
                orig = get_orig_from_offset(node, self.document)
            if orig is not None and not is_symbol(orig):
                self.toggle_visibility(node, orig)


if __name__ == '__main__':
    myeffect = HideShowOriginal()
    myeffect.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99

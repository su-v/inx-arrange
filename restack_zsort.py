#!/usr/bin/env python 
"""
Change Z-Order of selected objects or within selected group

Copyright (C) 2015 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local modules
import inkex
from pathmodifier import zSort
# system modules
import random


try:
    inkex.localize()
except:
    import gettext
    _ = gettext.gettext


class RestackZSort(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("--zsort",
                                     action="store", type="string",
                                     dest="zsort", default="rev",
                                     help="Restack mode based on Z-Order")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def restack_z_order(self):
        parentnode = None
        objects = []
        if len(self.selected) == 1:
            firstobject = self.selected[self.options.ids[0]]
            if firstobject.tag == inkex.addNS('g', 'svg'):
                parentnode = firstobject
                for child in parentnode.iterchildren(reversed=False):
                    objects.append(child)
        else:
            parentnode = self.current_layer
            for id_ in zSort(self.document.getroot(), self.selected.keys()):
                objects.append(self.selected[id_])
        if self.options.zsort == "rev":
            objects.reverse()
        elif self.options.zsort == "rand":
            random.shuffle(objects)
        if parentnode is not None:
            for item in objects:
                parentnode.append(item)

    def effect(self):
        if self.options.tab == '"help"':
            pass
        elif len(self.selected) > 0:
            self.restack_z_order()
        else:
            inkex.errormsg(_("There is no selection to restack."))


if __name__ == '__main__':
    e = RestackZSort()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
